from Objects.TwitterLogger import TwitterLogger
from flask import Flask
app = Flask(__name__, static_url_path='/assets')


def main():
    logger = TwitterLogger().logger
    logger.info('Twitter is running')
    app.run()


@app.route('/')
def get_posts():
    return app.send_static_file('/html/index.html')
if __name__ == '__main__':
    main()