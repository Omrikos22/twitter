import pymysql
from datetime import datetime
from Consts import Consts
from Queries import Queries
from Objects.TwitterLogger import TwitterLogger
from Objects.TwitterPost import TwitterPost

class PostDAL():
    def __init__(self):
        self.host = Consts.TWITTER_DB_HOST
        self.port = Consts.TWITTER_DB_PORT
        self.db_user = Consts.TWITTER_DB_USER
        self.db_password = Consts.TWITTER_DB_PASSWORD
        self.db_name = Consts.TWITTER_DB_NAME
        self.cur = None
        self.logger = TwitterLogger.logging.getLogger('twitter_logger')

    def connect(self):
        conn = pymysql.connect(host=self.host, port=self.port, user=self.db_user,passwd=self.db_password,db=self.db_name)
        self.cur = conn.cursor()

    def get_posts(self):
        posts = []
        try:
            results = self.cur.execute(Queries.GET_POSTS_QUERY)
            self.logger.INFO("Successfully get all posts from the DB")
        except Exception as exc:
            self.logger.ERROR("Didn't succeed to get all posts from the DB, Error occurred:{0}".format(exc))
        for result in results:
            posts.append(TwitterPost(result[Consts.POST_DB_TIME_INDEX], result[Consts.POST_DB_CONTENT_INDEX]))
        return posts

    def upload_post(self, post_content):
        try:
            self.cur.execute(Queries.INSERT_POSTS_QUERY) % datetime.now(), post_content
            self.logger,INFO("New post had arrived to system:{0}".format(post_content))
        except Exception as exc:
            self.logger.ERROR("Didn't succeed to insert new post, Error occurred:{0}".format(exc))



